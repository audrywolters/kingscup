//
//  CardData.m
//  KingsCup
//
//  Created by Audry Wolters on 6/6/14.
//  Copyright (c) 2014 audrywolters. All rights reserved.
//

#import "CardData.h"
@implementation CardData

- (void) getCardData
{
    self.suits = [NSArray arrayWithObjects:
                  [UIImage imageNamed:@"hearts.png"],
                  [UIImage imageNamed:@"diamonds.png"],
                  [UIImage imageNamed:@"clubs.png"],
                  [UIImage imageNamed:@"spades.png"],
                  nil];
    
    self.suitStrings = @[@"H",
                        @"D",
                        @"C",
                        @"S"];
    
    self.faces = @[@"A",
                   @"2",
                   @"3",
                   @"4",
                   @"5",
                   @"6",
                   @"7",
                   @"8",
                   @"9",
                   @"10",
                   @"J",
                   @"Q",
                   @"K"];
    
    self.titles = @[@"I’ve Never",
                    @"Story",
                    @"Charades",
                    @"Guess the Drawing",
                    @"Rule",
                    @"Truth or Dare",
                    @"Dance Party",
                    @"Drink Mate",
                    @"Rhyme",
                    @"Categories",
                    @"Boy Likers Drink",
                    @"Girl Likers Drink",
                    @"King's Cup"];
    
    self.actions = @[@"everyone hold up 3 fingers. go in a circle naming things you've never done. if a player has done that they must lower a finger. first player to run out of fingers must drink! ",
                          @"say a word. the next player repeats that word and adds one of their own. go in a circle repeating the whole story until someone messes up. that player must drink!",
                          @"act out a random word. if other players cannot guess that word in 1 minute, the actor must drink!",
                          @"draw a picture of \n a random word. \n if players cannot guess that word in \n 1 minute, the drawer must drink!",
                          @"make a rule to \n follow for the \n rest of the game \n i.e. drink with left hand, no first names. rule breakers must drink!",
                          @"player to draw this card must choose truth or dare. if player won't answer they must drink!",
                          @"everybody dance!",
                          @"tap the square of the player that will drink when you do.",
                          @"say a word. go in a circle rhyming that word until a player messes up. that player must drink!",
                          @"name a category of things i.e. metal bands. go in a circle naming items of said category until someone messes up. that player must drink!",
                          @"if you like boys \n (you know, \n in that way) \n drink!",
                          @"if you like girls \n (you know, \n in that way) \n drink!",
                          @"pour some of your drink into that big cup on the table. \n last person to draw a king must drink \n the whole cup!"];
}






- (NSMutableArray *)drawingWords
{
    if (!_drawingWords) {
      
        _drawingWords = [NSMutableArray arrayWithObjects:
                         @"Diarrhea",
                         @"Point",
                         @"Party",
                         @"Death Star",
                         @"Business",
                         @"Bobsled",
                         @"Frankenstein",
                         @"String",
                         @"Cage",
                         @"Spider Man",
                         @"Whisk",
                         @"Oar",
                         @"Popcorn",
                         @"Butter",
                         @"Lipstick",
                         @"Soap",
                         @"Story",
                         @"Sip",
                         @"Wax",
                         @"Scarf",
                         @"Fly",
                         @"Peel",
                         @"Sheet",
                         @"Electricity",
                         @"Leash",
                         @"Wash",
                         @"Year",
                         @"Chalk",
                         @"Hello",
                         @"Grass",
                         @"Cloth",
                         @"Kick",
                         @"Trip",
                         @"Beast",
                         @"Cuff",
                         @"Dryer",
                         @"Ticket",
                         @"Vegetable",
                         @"Marble",
                         @"Softball",
                         nil];
    }
    
    return _drawingWords;
}









- (NSMutableArray *)charadesWords
{
    if (!_charadesWords) {
        _charadesWords = [NSMutableArray arrayWithObjects:@"Ping Pong",
                           @"Torch",
                           @"Roof",
                           @"Cowboy",
                           @"Tomb Raider",
                           @"Carrot",
                           @"Bear",
                           @"Spider web",
                           @"Beggar",
                           @"Lung",
                           @"Flamingo",
                           @"Penguin",
                           @"Banana Peel",
                           @"Volcano",
                           @"Earthquake",
                           @"Road",
                           @"Rain",
                           @"Alarm Clock",
                           @"Quicksand",
                           @"Pajamas",
                           @"Poison",
                           @"Nightmare",
                           @"Badge",
                           @"Mirror",
                           @"Puppy",
                           @"Outer Space",
                           @"Shadow",
                           @"Chess",
                           @"Sunburn",
                           @"Scale",
                           @"Giraffe",
                           @"Bonnet",
                           @"Bedbug",
                           @"Worm",
                           @"Easel",
                           @"Throne",
                           @"Lawn mower",
                           @"Remote Control",
                           @"Dentist",
                           @"Mouse",
                          nil];
    }
    return _charadesWords;
}



@end
